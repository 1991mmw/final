package pl.sda.service;

import java.util.List;

import org.springframework.stereotype.Service;

import pl.sda.model.User;

@Service
public interface UserService {
	public void addUser(User user);
	public void updateUser(User user);
	public List<User> listUsers();
	public User getById(Long id);
	public void removeUser(Long id);
}
