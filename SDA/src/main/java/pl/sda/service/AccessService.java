package pl.sda.service;

import org.springframework.stereotype.Service;

import pl.sda.model.Access;

@Service
public interface AccessService {
	public boolean checkAccess(String username, String password, Access access);
}
