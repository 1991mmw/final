package pl.sda.dao;

import java.util.List;

import pl.sda.model.Delivery;
import pl.sda.model.User;

public interface UserDao {
	
	//crud users
	public void addUser(User user);
	public void updateUser(User user);
	public List<User> listUsers();
	public void removeUser(Long id);
	
	//find user by id
	public User getById(Long id);
	
	//
	
	public User getByUsername(String username);
	public void createDeliveryRequest(Delivery delivery);
	public List<Delivery> checkDelivery(Long id);
	
	
	
}
