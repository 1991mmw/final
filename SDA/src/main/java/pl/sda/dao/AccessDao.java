package pl.sda.dao;

import pl.sda.model.Access;

public interface AccessDao {
	public boolean checkAccess(String username, String password, Access access);
}
