package pl.sda.dao;

public interface LoginDao {
	public boolean checkLogin(String username, String password);
}
