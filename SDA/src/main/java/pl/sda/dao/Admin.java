package pl.sda.dao;

import java.util.List;

import pl.sda.model.Delivery;
import pl.sda.model.User;

public interface Admin {
	
	//crud users
	public void addUser(User user);
	public void updateUser(User user);
	public void removeUser(Long id);
	public List<User> listUsers();
	
	//find user by id
	public User getByUserId(Long id);
	
	//crud courier
	public void addCourier(User user);
	public void updateCourier(User user);
	public void removeCourier(User user);
	public List<User> listCouriers();
	
	//find courier by id
	public User getByCourierId(Long id);
	public void checkAllDeliveries(Delivery delivery); //select from delivery table
	
	
}
