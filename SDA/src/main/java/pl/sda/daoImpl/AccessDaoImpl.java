package pl.sda.daoImpl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import pl.sda.dao.AccessDao;
import pl.sda.model.Access;

@Repository
public class AccessDaoImpl implements AccessDao{
	
	@Resource(name="sessionFactory")
    protected SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
           this.sessionFactory = sessionFactory;
    }
   
    protected Session getSession(){
           return sessionFactory.openSession();
    }

	@Override
	public boolean checkAccess(String username, String password, Access access) {
		System.out.println("Checking Access Level");
		Session session = sessionFactory.openSession();
		boolean loginAccess = false;
		String HQL_QUERY ="select o.username, o.password, o.access from User as o where o.username=? and o.password=? and o.access=?";
		Query query = session.createQuery(HQL_QUERY);
		query.setParameter(0,username);
		query.setParameter(1,password);
		query.setParameter(2,access);
		List list = query.list();

		if ((list != null) && (list.size() > 0)) {
			loginAccess = true;
		}

		session.close();
		return loginAccess;
	}

}
