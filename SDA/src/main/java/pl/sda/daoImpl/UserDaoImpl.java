package pl.sda.daoImpl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import pl.sda.dao.UserDao;
import pl.sda.model.Delivery;
import pl.sda.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	@Resource(name = "sessionFactory")
	protected SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session getSession() {
		return sessionFactory.openSession();
	}

	@Override
	public void addUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(user);
		System.out.println("User saved");
	}

	@Override
	public void updateUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(user);
		System.out.println("User updated");
	}

	@Override
	public List<User> listUsers() {
		Session session = this.sessionFactory.getCurrentSession();
		List<User> list = session.createQuery("from User").list();
		for (User user : list) {
			System.out.println("User list: " + user);
		}
		return list;
	}

	@Override
	public User getById(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.load(User.class, new Long(id));
		return user;
	}

	@Override
	public void removeUser(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.load(User.class, new Long(id));
		if (null != user) {
			session.delete(user);
		}
		System.out.println("User deleted");
	}

	@Override
	public User getByUsername(String username) {
		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.load(User.class, new String(username));
		return user;
	}

	@Override
	public void createDeliveryRequest(Delivery delivery) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(delivery);
		System.out.println("Delivery request made");
	}

	@Override
	public List<Delivery> checkDelivery(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Delivery> list = (List<Delivery>) session.load(Delivery.class, new Long(id));
			return list;
	}

}
