package pl.sda.model;

public enum Access {
	user,
	admin,
	freight_forwarder,
	courier

}
