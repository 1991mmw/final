package pl.sda.controller;

import java.lang.reflect.Method;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.sda.form.LoginForm;
import pl.sda.model.User;
import pl.sda.service.LoginService;

@Controller
@RequestMapping("login.html")
public class LoginController {

	@Autowired
	public LoginService loginService;

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Map model) {
		User user = new User();
		model.put("user", user);
		return "login";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(@Valid User user, BindingResult result, Map model) {
		if (result.hasErrors()) {
			return "login";
		}

		/*
		 * String username = "Username"; String password = "password"; loginForm
		 * = (LoginForm) model.get("loginForm"); if
		 * (!loginForm.getUsername().equals(username) ||
		 * !loginForm.getPassword().equals(password)) { return "loginform"; }
		 */

		boolean userExists = loginService.checkLogin(user.getUsername(),user.getPassword());
		if (userExists) {
			model.put("user", user);
			return "loginsuccess";
		} else {
			result.rejectValue("username", "invaliduser");
			return "login";
		}
	}
}
