package pl.sda.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.sda.model.User;
import pl.sda.service.AccessService;

@Controller
@RequestMapping("loginsuccess.html")
public class AccessController {

	@Autowired
	private AccessService accessService;

	@RequestMapping(method = RequestMethod.POST)
	public String checkAccess(@Valid User user, BindingResult result, Map model) {
		boolean access = accessService.checkAccess(user.getUsername(), user.getPassword(), user.getAccess());
		if(access && user.getAccess().equals("admin")){
			return "admin";
		}else if(access && user.getAccess().equals("freight_forwarder")){
			return "ff";
		}else if(access && user.getAccess().equals("courier")){
			return "courier";
		}else{
			return "customer";
		}
	}

}
