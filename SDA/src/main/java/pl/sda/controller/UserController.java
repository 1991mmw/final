package pl.sda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.sda.model.User;
import pl.sda.service.UserService;

@Controller
@RequestMapping("user.html")
public class UserController {

	@Autowired
	private UserService userService;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String listUsers(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("listUsers", this.userService.listUsers());
		return "user";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("user") User user) {
		if (user.getId() == 0) {
			this.userService.addUser(user);
		} else {
			this.userService.updateUser(user);
		}
		return "redirect:/user.html";
	}

	@RequestMapping("/remove/{id}")
	public String removeUser(@PathVariable("id") Long id) {

		this.userService.removeUser(id);
		return "redirect:/user";
	}

	@RequestMapping("/edit/{id}")
	public String editPerson(@PathVariable("id") Long id, Model model) {
		model.addAttribute("user", this.userService.getById(id));
		model.addAttribute("listUsers", this.userService.listUsers());
		return "user";
	}

}
