package pl.sda.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.sda.dao.LoginDao;
import pl.sda.service.LoginService;

@Service("loginService")
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	private LoginDao loginDao;
	
	
	public void setLoginDao(LoginDao loginDao) {
		this.loginDao = loginDao;
	}


	@Override
	public boolean checkLogin(String username, String password) {
		System.out.println("In service class checking credentials");
		return loginDao.checkLogin(username, password);
	}

}
