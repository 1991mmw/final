package pl.sda.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.sda.dao.AccessDao;
import pl.sda.model.Access;
import pl.sda.service.AccessService;

@Service("accessService")
public class AccessServiceImpl implements AccessService{
	
	@Autowired
	private AccessDao accessDao;
	
	public AccessDao getAccessDao() {
		return accessDao;
	}

	public void setAccessDao(AccessDao accessDao) {
		this.accessDao = accessDao;
	}

	@Override
	public boolean checkAccess(String username, String password, Access access) {
		System.out.println("In service class checking access level");
		return accessDao.checkAccess(username, password, access);
	}
}
