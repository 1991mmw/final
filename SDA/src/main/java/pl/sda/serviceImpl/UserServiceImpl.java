package pl.sda.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.sda.dao.UserDao;
import pl.sda.model.User;
import pl.sda.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDao userDao;

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	@Transactional
	public void addUser(User user) {
		this.userDao.addUser(user);
	}

	@Override
	@Transactional
	public void updateUser(User user) {
		this.userDao.updateUser(user);
	}

	@Override
	@Transactional
	public List<User> listUsers() {
		return this.userDao.listUsers();
	}

	@Override
	@Transactional
	public User getById(Long id) {
		return this.userDao.getById(id);
	}

	@Override
	@Transactional
	public void removeUser(Long id) {
		this.userDao.removeUser(id);
	}

}
