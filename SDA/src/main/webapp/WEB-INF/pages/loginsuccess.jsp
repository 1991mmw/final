<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Page</title>
</head>
<body>
	<h1>Welcome User</h1>
	<c:url var="access" value="loginsuccess.html"></c:url>
	<form:form action="${checkAccess}" commandName="user">
	<c:if test="${checkAccess == admin}">
		<c:redirect url="admin.html"></c:redirect>
	</c:if>
	<c:if test="${checkAccess == freight_forwarder}">
		<c:redirect url="ff.html"></c:redirect>
	</c:if>
	<c:if test="${checkAccess == courier}">
		<c:redirect url="courier.html"></c:redirect>
	</c:if>
	<c:if test="${checkAccess == user}">
		<c:redirect url="customer.html"></c:redirect>
	</c:if>
	</form:form>
</body>
</html>